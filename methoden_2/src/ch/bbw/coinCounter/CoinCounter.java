package ch.bbw.coinCounter;

import java.util.Scanner;

public class CoinCounter {
	Scanner scn = new Scanner(System.in);
	float amount;
	int coinFive = 0;
	int coinTwo = 0;
	int coinOne = 0;
	int halfCoin = 0;
	int twentyCoin = 0;
	int tenCoin = 0;
	int fiveCoin = 0;
	String output = "";
	public CoinCounter() {
		System.out.print("Geben Sie einen Betrag ein: ");
		amount = scn.nextFloat();
		System.out.println("Für diesen Betrag benötigen Sie:" + "\n" + countCoins(amount));
	}
	public String countCoins(float amount) {
		while (amount >= 0.05) {
			if (amount - 5 >= 0) {
				amount -= 5;
				coinFive++;
			}
			else if (amount - 2 >= 0) {
				amount -= 2;
				coinTwo++;
			}
			else if (amount >= 1) {
				amount --;
				coinOne++;
			}
			else if (amount >= 0.5) {
				amount -= 0.5;
				halfCoin++;
			}
			else if (amount >= 0.2) {
				amount -= 0.2;
				twentyCoin++;
			}
			else if (amount >= 0.1) {
				amount -= 0.1;
				tenCoin++;
			}
			else if (amount >= 0.05) {
				amount -= 0.05;
				fiveCoin++;
			}
		}
		output = "Fünfliber: " + coinFive + "\n";
		output += "Zweifränkler: " + coinTwo + "\n";
		output += "Einfränkler: " + coinOne + "\n";
		output += "Fünfziger: " + halfCoin + "\n";
		output += "Zwanziger: " + twentyCoin + "\n";
		output += "Zehner: " + tenCoin + "\n";
		output += "Fünfer: " + fiveCoin + "\n";
		return output;
	}
}
